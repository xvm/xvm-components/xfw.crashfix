// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Includes
//

// XFW.CrashFix
#include "patch.h"
#include "patch_1.h"
#include "helper.h"



//
// DATA
//

// IDA search: 48 89 5C 24 08 48 89 6C 24 10 48 89 74 24 18 57 41 54 41 55 41 56 41 57 48 83 EC 20 48 8D 05 ? ? ? ? 48 8B F1
static const char* function_signature = "\x48\x89\x5C\x24\x08\x48\x89\x6C\x24\x10\x48\x89\x74\x24\x18\x57\x41\x54\x41\x55\x41\x56\x41\x57\x48\x83\xEC\x20\x48\x8D\x05\xFF\xFF\xFF\xFF\x48\x8B\xF1";
static const char* function_signature_mask = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx????xxx";

static const size_t replace_addr_offset = 0x1F2;
static const char replace_addr_test = 0x48;

static const size_t return_addr_offset = 0x20B;
static const char return_addr_test = 0x45;



//
// ASM
//

extern "C" size_t patch_1_replaceaddr;
extern "C" size_t patch_1_returnaddr;
extern "C" void patch_1_asmfunc();



//
// Functions
//

int patch_1_apply()
{
    //init search
    auto[startpos, endpos] = GetExeAddressRange();

    char *test = NULL;

    size_t crashfunction_addr = FindFunction(startpos, endpos, function_signature, function_signature_mask);
    if (crashfunction_addr == 0) {
        return -1;
    }

    patch_1_replaceaddr = crashfunction_addr + replace_addr_offset;
    test = reinterpret_cast<char*>(patch_1_replaceaddr);
    if (test[0] != replace_addr_test) {
        return -2;
    }

    patch_1_returnaddr = crashfunction_addr + return_addr_offset;
    test = reinterpret_cast<char*>(patch_1_returnaddr);
    if (test[0] != return_addr_test) {
        return -3;
    }

    make_jmp(reinterpret_cast<void*>(patch_1_replaceaddr), patch_1_asmfunc);
    return 0;
}
