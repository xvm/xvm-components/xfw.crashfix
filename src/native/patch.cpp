// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Includes
//

// Windows
#include <Windows.h>

// XFW.CrashFix
#include "patch.h"
#include "patch_common.h"
#include "patch_1.h"
#include "patch_2.h"


//
// Functions
//

//requires 13 bytes in the original function
//requires pop rax at the beggining of your replacement code
bool make_jmp(void* originAddress, void* replacementAddress)
{
    char* originFunction = nullptr;
    DWORD dwProtect = 0;

    originFunction = reinterpret_cast<char*>(originAddress);
    VirtualProtect(originFunction, 13, PAGE_EXECUTE_READWRITE, &dwProtect);

    //push rax
    originFunction[0] = 0x50;

    //mov rax, QWORD
    originFunction[1] = 0x48;
    originFunction[2] = 0xB8;
    memcpy(originFunction + 3, &replacementAddress, 8);

    //jmp rax
    originFunction[11] = 0xFF;
    originFunction[12] = 0xE0;

    VirtualProtect(originFunction, 13, dwProtect, &dwProtect);

    return true;
}


int patch_get_count() {
    return 2;
}


int patch_apply(int i) {
    int result = -100;

    switch (i) {
    case 1:
        result = patch_1_apply();
        break;
    case 2:
        result = patch_2_apply();
        break;
    default:
        break;
    }

    return result;
}
