// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Includes
//

// stdlib
#include <cstddef>


//
// Functions
//

int patch_get_count();

int patch_apply(int patch_num);
