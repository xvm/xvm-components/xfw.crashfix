// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Includes
//

// stdlib
#include <cstddef>
#include <string>
#include <utility>

//
// Functions
//

std::wstring GetExeFilename();

std::pair<size_t, size_t> GetExeAddressRange();

size_t GetModuleSize(const wchar_t* filename);

bool DataCompare(const char* pData, const char* bMask, const char* szMask);

size_t FindFunction(size_t startpos, size_t endpos, const char* pattern, const char* mask);
