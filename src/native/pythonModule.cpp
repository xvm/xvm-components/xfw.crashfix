﻿// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Imports
//

// python
#include <pybind11/pybind11.h>

// XFW.CrashFix
#include "patch_common.h"



//
// Module
//

PYBIND11_MODULE(XFW_CrashFix, m) {
    m.doc() = "XFW CrashFix module";
    
    m.def("fix_count", &patch_get_count);
    m.def("fix_apply", &patch_apply);
}

