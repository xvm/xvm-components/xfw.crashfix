// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

#pragma once

bool make_jmp(void* originAddress, void* replacementAddress);
