// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Includes
//

// XFW.CrashFix
#include "patch.h"
#include "patch_1.h"
#include "helper.h"



//
// DATA
//

//IDA search: 40 56 41 55 41 57 48 83 EC 30 4C 8B B9 E8 4C 00 00
              
static const char* function_signature = "\x40\x56\x41\x55\x41\x57\x48\x83\xEC\x30\x4C\x8B\xB9\xE8\x4C\x00\x00";
static const char* function_signature_mask = "xxxxxxxxxxxxxxxxx";

static const size_t replace_addr_offset = 0x11C;
static const char replace_addr_test = 0x48;

static const size_t return_addr_offset = 0x131;
static const char return_addr_test = 0x75;



//
// ASM
//

extern "C" size_t patch_2_replaceaddr;
extern "C" size_t patch_2_returnaddr;
extern "C" void patch_2_asmfunc();



//
// Functions
//

int patch_2_apply()
{
    //init search
    auto[startpos, endpos] = GetExeAddressRange();

    char *test = NULL;

    size_t crashfunction_addr = FindFunction(startpos, endpos, function_signature, function_signature_mask);
    if (crashfunction_addr == 0) {
        return -1;
    }

    patch_2_replaceaddr = crashfunction_addr + replace_addr_offset;
    test = reinterpret_cast<char*>(patch_2_replaceaddr);
    if (test[0] != replace_addr_test) {
        return -2;
    }

    patch_2_returnaddr = crashfunction_addr + return_addr_offset;
    test = reinterpret_cast<char*>(patch_2_returnaddr);
    if (test[0] != return_addr_test) {
        return -3;
    }

    make_jmp(reinterpret_cast<void*>(patch_2_replaceaddr), patch_2_asmfunc);
    return 0;
}
