// SPDX-License-Identifier: MIT
// Copyright (c) 2018-2022 XVM Contributors

//
// Includes
//

// Windows
#include <Windows.h>
#include <Tlhelp32.h>
#include <process.h>

// XFW.CrashFix
#include "helper.h"



//
// Functions
//

std::wstring GetExeFilename(){
    wchar_t lpFilename[2048]{};
    GetModuleFileNameW(NULL, lpFilename, std::size(lpFilename));
    return {lpFilename};
}


std::pair<size_t, size_t> GetExeAddressRange(){
    auto filename = GetExeFilename();

    size_t startpos = (size_t)(GetModuleHandleW(filename.c_str()));
    size_t endpos = startpos + GetModuleSize(filename.c_str());

    return {startpos, endpos};
}

size_t GetModuleSize(const wchar_t* filename)
{
    MODULEENTRY32W moduleEntry;
    size_t moduleSize = 0;

    HANDLE hSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, _getpid());
    if (hSnap)
    {
        moduleEntry.dwSize = sizeof(MODULEENTRY32W);
        BOOL Result = Module32FirstW(hSnap, &moduleEntry);

        while (Result)
        {
            if (wcsstr(filename, moduleEntry.szModule))
            {
                moduleSize = moduleEntry.modBaseSize;
                break;
            }
            Result = Module32NextW(hSnap, &moduleEntry);
        }
        CloseHandle(hSnap);
    }

    return moduleSize;
}


bool DataCompare(const char* pData, const char* bMask, const char* szMask)
{
    for (; *szMask; ++szMask, ++pData, ++bMask) {
        if (*szMask == 'x' && *pData != *bMask)    {
            return FALSE;
        }
    }

    return TRUE;
}


size_t FindFunction(size_t startpos, size_t endpos, const char* pattern, const char* mask)
{
    for (size_t pos = startpos; pos < endpos; pos++) {
        if (DataCompare((const char*)pos, pattern, mask) == TRUE) {
            return pos;
        }
    }

    return 0;
}
