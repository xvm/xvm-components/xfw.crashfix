"""
SPDX-License-Identifier: MIT
Copyright (c) 2018-2022 XVM Contributors
"""

#
# Imports
#

# stdlib
import logging
import os

# xfw.native
from xfw_native.python import XFWNativeModuleWrapper



#
# Globals
#

__native = None
__initialized = False



#
# XFW Loader
#

def xfw_is_module_loaded():
    global __initialized
    return __initialized


def xfw_module_init():
    global __native
    global __initialized

    if __native is None:
        __native = XFWNativeModuleWrapper('com.modxvm.xfw.crashfix', 'xfw_crashfix.pyd', 'XFW_CrashFix')

        logger = logging.getLogger('XFW/CrashFix')
        logger.info("init: applying crashfixes")

        for bf_num in range(1, __native.fix_count() + 1):
            err_code = __native.fix_apply(bf_num)
            if err_code >= 0:
                logger.info("init: BugFix %i: OK, %i" % (bf_num, err_code))
            else:
                logger.warning("init: BugFix %i: FAIL, %i" % (bf_num, err_code))

        __initialized = True


def xfw_module_fini():
    global __native
    __native = None
