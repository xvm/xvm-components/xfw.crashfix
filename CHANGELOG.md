# Changelog

## v11.1.0

* MT 1.31.0 support

## v11.0.0

* MT 1.30.0 support

## v10.1.0

* Update for WoT 1.19.1

## v10.0.0

* First release as separate package